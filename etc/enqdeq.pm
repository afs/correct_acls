#
# file-system-based locks
#	enq("string") - process (!) acquires lock on string
#       deq("string") - process releases lock
#
# if lock held (and holding process still exists), waits and retries periodically
# maximum lock hold time: arbitrarily 1 day - to change see $max_lock_time
# locks recorded in /tmp - can be changed see $enq_lock_dir
#
# 1.1.1970 (approx) Rainer Toebbicke, first version
#

# unlink lock files. This has to be done an 'END' clause as 'volmove' may exit/die.
END {

	foreach my $k (keys %enq_locks_glob) {
		unlink $enq_locks_glob{$k};
	}
	foreach my $k (keys %enq_locks_tmp) {
		unlink $enq_locks_tmp{$k};
	}
}


sub enq {
	my ($res, $max_lock_time, $return_error)=@_;

	my $enq_lock_dir="/tmp";
	$max_lock_time=24*3600 unless defined($max_lock_time);

	my $lockfile="$enq_lock_dir/lock_$res";
	$enq_locks_tmp{$res}="$lockfile.$$";

	unlink $enq_locks_tmp{$res};
	open(L,">$enq_locks_tmp{$res}") || die "Cannot open $enq_locks_tmp{$res}: $!";
	$now=localtime;
	print L "$now\n$$\n";
	close(L);

	while (!link($enq_locks_tmp{$res},$lockfile)) {
		if ( (time-(stat($lockfile))[9]) > $max_lock_time ) {
		    if ($return_error) {
			warn "error, failed to lock $lockfile within ${max_lock_time}s";
			unlink $enq_locks_tmp{$res};
			return(0);
		    }

		    warn "$lockfile expired, unlocking";
		    unlink $lockfile || die "Cannot unlink $lockfile: $!";
		    next;
		}
		open(L, $lockfile);
		my @lines=<L>;
		close(L);
		my ($heldby)=$lines[1]=~/^(\d+)$/;
		if ($heldby+0 > 1 && -d "/proc") {
		    if (!-l "/proc/$heldby/cwd" && -l "/proc/$$/cwd") {	# the other guy must have crashed
			unlink $lockfile || die "Cannot unlink $lockfile: $!";
		    	next;
		    }
		}
		if (! kill(0, $heldby)) {	# easier way to check that the other hasn't disappeared
		    unlink $lockfile || die "Cannot unlink $lockfile: $!";
		    next;
		}
		system "/bin/sleep", 10+int(rand(20));
	}

	# obtained the lock, make sure release it eventually
	$enq_locks_glob{$res}="$lockfile";		# now only can make it global!
	
	return(1);
}

sub deq {
	my($res)=@_;
	unlink $enq_locks_tmp{$res};
	if (exists($enq_locks_glob{$res})) { unlink $enq_locks_glob{$res}; }
}

1;
