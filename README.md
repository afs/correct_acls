# Correct Acls

This repo contains part* of the scripts used to check and correct the ACLs applied to user directories, in order to comply with the Security Rules.

For now it only contains the wrapper, which gets all users in the system and afterwards calls the real script that still lives in AFS itself (`/afs/cern.ch/project/afs/cernafs/etc/correct_acls`). This script and all its dependencies should also be copied over and then this location in AFS should be a git clone of this repo.