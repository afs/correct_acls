#-------------------------------------------------------------------------------
# AFSLDAP.pm
#
# Module for LDAP queries (and some helper routines).
#
# Initial version by Arne Wiebalck IT-DSS/FDO, 2010.
#-------------------------------------------------------------------------------

package AFSLDAP;

use strict;
use warnings;
use Net::LDAP;

my $LDAPserver = 'xldap.cern.ch';
my $domain_root = 'DC=cern,DC=ch';

#-------------------------------------------------------------------------------
#
# Interface functions
#
#-------------------------------------------------------------------------------

sub is_LDAP_item {

    # check if an LDAP item exists
    #
    # parameters: the item name, the object class
    #
    # returns   : 1 if the item exists in the given class
    #             0 otherwise

    my ($item, $class) = @_;

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    print "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
			     filter => "(&(cn=$item)(objectClass=$class))");
    if ($mesg->code) {
	print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind;

    # there should be only one entry from our search
    return 1 if ($mesg->shift_entry);
    return 0;
}

sub is_LDAP_objectClass {

    # check if an LDAP item is a from a given class
    #
    # parameters: the item name, the class to check
    #
    # returns   : 1 if the item exists
    #             0 otherwise

    my ($item, $class) = @_;

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    print "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(cn=$item)",
                             attrs  => [ "objectClass" ]);
    if ($mesg->code) {
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind;

    # check the class
    my $entry = $mesg->shift_entry;
    if (defined $entry) {
    	foreach my $c ($entry->get("objectClass")) {
        	return 1 if ($c eq $class)
    	}
    }
    return 0;
}


sub get_egroup_members {

    # get the members of an e-group group from LDAP
    #
    # parameters: the group name
    #
    # returns   : an array with the group members

    my $grp = shift;
    my $ldap = Net::LDAP->new($LDAPserver) or die "$@";

    # do the search
    my $mesg;
    my $step = 1499;
    my $low  = 0;
    my $high = $step;
    my @Amem = ();
    my $len  = 0;
    my $continue = 1;

    while ($continue) {

	$mesg = $ldap->search(base   => $domain_root,
			      filter => "(&(CN=$grp)(objectClass=group))",
			      attrs  => [ "member;range=$low-$high" ] );
	if ($mesg->code) {
	    die "LDAP search error: ".$mesg->error."\n";
	}
	if (1 == $mesg->count) {
	    my $entry = $mesg->shift_entry;
	    my @Atmp = $entry->get("member;range=$low-$high");
	    my $len = scalar @Atmp;

	    if ($len) {
		push @Amem, $entry->get("member;range=$low-$high");
		$low = $high + 1;
		$high = $low + $step;
	    } else {
		push @Amem, $entry->get("member;range=$low-*");
		$continue = 0;
	    }
	} else {
	    $continue = 0;
	}
    }

    # disconnect
    $ldap->unbind;
    return @Amem;
}

sub get_egroup_members_flat {

    # flatten out a given e-group recursively
    #
    # parameters: an e-group name
    #             the reference to the flat list
    #             the reference to a memory hash
    #
    # returns   : nothing

    my ($eg, $ar, $hr) = @_;

    # protect from deep recursion
    if (exists $$hr{$eg}) {
        return;
    } else {
        $$hr{$eg} = 1;
    }

    # traverse the n-ary tree
    my @A = &get_egroup_members($eg);
    my @B = &AFSLDAP::extract_CNs_from_DNs(@A);
    if (0 == @B) {
        # omit non user leafs, such as empty groups
        if (&is_LDAP_objectClass($eg, "user")) {
            push @$ar, $eg;
        } else {
            print STDERR "Found non-user $eg as a leaf\n";
        }
    } else {
        foreach my $pg (@B) {
            &get_egroup_members_flat($pg, $ar, $hr);
        }
    }
    return;
}


sub extract_CNs_from_DNs {

    # extract the CNs from an array of DNs
    #
    # parameters: an array with DNs
    #
    # returns   : an array with the CNs

    my @ADNs = @_;
    my @ACNs = ();

    foreach my $dn (@ADNs) {
	if ((split(/,/, $dn))[0] =~ /^CN=(.*)$/) {
	    push @ACNs, $1;
	} else {
	    print STDERR "Could not identify CN in in DN: $dn\n";
	    next;
	}
    }
    return @ACNs;
}

sub get_mail_address {

    # get the mail address for an account
    #
    # parameters: an account name
    #
    # returns   : a mail address or an empty string if
    #             the object was not found in LDAP

    my $account = shift;

    if ($account !~ /^[a-z][a-z0-9_-]+$/) {
	print STDERR "Illegal account name \'$account\'\n";
	return "";
    }

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    print "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(&(objectCategory=Person)(cn=$account))",
                             attrs  => [ "mail" ]);
    if ($mesg->code) {
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind;

    # get the mail address
    if (1 == $mesg->count) {
	my $entry = $mesg->shift_entry;
	return ($entry->get("mail"))[0];
    } else {
	print STDERR "Unable to find mail address for $account\n";
	return "";
    }
}

sub get_name {

    # get the user's name for an account
    #
    # parameters: an account name
    #
    # returns   : a string containing given name and surname

    my $account = shift;

    if ($account !~ /^[a-z][a-z0-9_-]+$/) {
	print STDERR "Illegal account name \'$account\'\n";
	return "";
    }

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    print "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(&(objectCategory=Person)(cn=$account))",
                             attrs  => [ "givenName", "sn" ]);
    if ($mesg->code) {
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind;

    # get the name
    if (1 == $mesg->count) {
	my $entry = $mesg->shift_entry;
	return (join(" ", $entry->get("givenName"),  $entry->get("sn")));
    } else {
	print STDERR "Unable to find name for $account\n";
	return "";
    }
}

sub get_language {

    # get the user's preferred language for an account
    #
    # parameters: an account name
    #
    # returns   : a string "EN"/"FR" (or anything else??)

    my $account = shift;

    if ($account !~ /^[a-z][a-z0-9_-]+$/) {
	print STDERR "Illegal account name \'$account\'\n";
	return "";
    }

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    print "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(&(objectCategory=Person)(cn=$account))",
                             attrs  => [ "preferredLanguage"]);
    if ($mesg->code) {
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind;

    # get the name
    if (1 == $mesg->count) {
	my $entry = $mesg->shift_entry;
        my @langs = $entry->get("preferredLanguage");  # can have several??
	return $langs[0];
    } else {
	print STDERR "Unable to find language for $account, assuming EN\n";
	return "EN";
    }
}


1;

__END__
