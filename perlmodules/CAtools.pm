# CAtools.pm - Cern Afs tools: routines to supplement some additional AFS tools
#
# Bernard Antoine 06/2003  
#
# $Header: /afs/cern.ch/project/afs/dev/misc/RCS/CAtools.pm,v 1.15 2010/12/08 13:54:16 rtb Exp $
# $Pgm:  /afs/cern.ch/project/afs/perlmodules
# $Man:  /afs/cern.ch/project/afs/doc
# $Html: /afs/cern.ch/project/afs/doc

package CAtools;

$user_addresses = "/afs/cern.ch/project/afs/MailAddr/user_addresses";

use strict;
use Exporter;
use vars qw (@ISA @EXPORT $VERSION 
            );
@ISA = qw(Exporter);
@EXPORT = qw ( _find_afs_cmd _get_IPports _timeout get_from_oracle MailAddr _cron_select
             );
$VERSION = 1.01;


sub _find_afs_cmd {
########## CERN specific :  /usr/sue/*, no more /afs/usr/local
    my(@paths) = qw( /usr/afs/bin /usr/bin /bin /usr/sbin /usr/lib
                     /usr/sue/bin /usr/sue/etc
                     /Library/OpenAFS/Tools/bin /Library/OpenAFS/Tools/etc
                   );
    my($cmd) = @_;
    my($d);
    foreach $d (@paths) {
        return "$d/$cmd" if (-x "$d/$cmd");
    }
    return undef;
}

sub _get_IPports {
    my(%IPport) = qw(
        file      7000
        callback  7001
        prot      7002
        vldb      7003
        kauth     7004
        vol       7005
        error     7006
        nanny     7007
        update    7008
        rmtsys    7009
        buserver  7021
    ) ;
    unless ( @_ ) {
        $IPport{'_fs_'} = [ 'file' , 'vol' , 'nanny' ];
        $IPport{'_db_'} = [ 'prot' , 'vldb' , 'kauth' , 'buserver' ];
    }
    return %IPport;
}


sub get_from_oracle {

    my ($select,$res,@res);
##  my $debug = ($ENV{'DEBUG'});
    my $ORACLE_CERN   = "/afs/cern.ch/project/oracle";
    my $ORACLE_HOME   = "$ORACLE_CERN/\@sys/prod";
    $ENV{ORACLE_HOME} = "$ORACLE_HOME" unless $ENV{ORACLE_HOME};
    $ENV{TNS_ADMIN}   = "$ORACLE_CERN/admin";
    $ENV{LD_LIBRARY_PATH} .= ":$ORACLE_HOME/lib" unless index($ENV{LD_LIBRARY_PATH}, "$ORACLE_HOME/lib") >=0;
    
    my $dbuser = 'afsmail/Bernard_Antoine@found';
    @res = ();
    
    ($select = "@_") =~ s/\n/ /g;
##  print "$select\n" if $debug;

    
    require DBI; import DBI;
    
    unless (defined $CAtools'dbh) {
        my $dbname = 'dbi:Oracle:';
        
        # connect to the database
        $CAtools'dbh = DBI->connect($dbname, $dbuser, '') or die "connecting : $DBI::errstr\n";
    }
    
    my $sth3 = $CAtools'dbh->prepare($select);
    my $rc =   $sth3->execute;
    while ( my @line = $sth3->fetchrow_array ) {
       push @res, @line, "\n";
    }
    $sth3->finish;
##  print "@res\n" if $debug;
    
    
    foreach (@res) { $_ =~ s/^\s*([^\s]+)\s*$/$1/ if defined $_ ; }
    return @res;
}


sub MailAddr {
    my($user) = "\L$_[0]\E" ;

    if (-r "$CAtools'user_addresses") {
       if (open(F,"<$CAtools'user_addresses")) {
          while ($_ = <F>) {
             if ($_ =~ /^$user\s\s(.*)$/) {
                close(F);
                return $1;              # which could be nullstring
             }
          }
          close(F);
          # else fall through call to Oracle
       }
    }
    
### now that saveMailAddr works, we no longer need a fancy failover
    my $pts = _find_afs_cmd('pts');
    system "$pts exam $user >/dev/null 2>/dev/null";
    return ($?) ? "" : "$user\@mail.cern.ch";
}

sub _cron_select {
   use vars qw($arc_server @__C_S @__C_L @__W_S @__W_L);
   require "/afs/cern.ch/project/afs/etc/afsconf.pl";     # brings in $arc_server @__C_S & @__C_L
   # since afsconf.pl may have been imbedded already, it will not come again., so variables
   # are not set.   So we have to bring such variables from main.
   # the case of @__C_x if a bit different: we copy it ANYWAY if it has already been set.
   if (@main::__C_S) {                      # already defined @__C_x win
      @__C_S = @main::__C_S;
      @__C_L = @main::__C_L;
   }

   # if this a machine in the AD realm, contact the front-end servers
   my $conf = "/etc/krb5.conf";
   if ($ENV{'KRB5_CONFIG'}) { $conf = $ENV{'KRB5_CONFIG'}; }
   open(F, "<$conf") || die "cannot read krb5 configuration file $conf\n" ;
   my $header = <F>;
   close F;
   if ($header =~ / AD /) {
      if (@main::__W_S) {                      # already defined @__W_x win
         @__W_S = @main::__W_S;
         @__W_L = @main::__W_L;
      }
      @__C_S = @__W_S;
      @__C_L = @__W_L;
   }

   $arc_server = $__C_S[0];      # quick fix for a problem with default $arc_server (==afsdb1)
##print "arc server=$arc_server, cron servers=(@__C_S), split at (@__C_L)\n";

   my $princ;
   if (@_) {
      $princ = shift @_;
      return (@__C_S) if $princ eq "*";
   } else {
     my($user) = getpwuid($<);
#    why use arc -h $arc_server whoami and not just parse `tokens.krb` or `klist` ?
#    well, when not used inside an AFS server, _cron_select is used by acrontab. On ANY server
#    'arc' must be installed, else acrontab cannot work, but tokens.krb might just not be there!
#    2 additionnal problems here:
#    - perl fails the arc command if we are only partially root, e.g. called from a script with su bit
#    - we have to force STDIN for arc whoami, else it will "eat" any pending input, 
#      as in "acrontab < list". But then arc may forget to return something! So better retry
     my($arc)  = _find_afs_cmd("arc");
     my $R = $<;
     if ($< != 0 and $> == 0) { $< = $>; }   # better be totally root in only partially so
     my $i = 10;                             # prepare for some number of retries
     my $answ;
     my($klog) = "/usr/sue/bin/kinit";
     while ( $i-- ) {
        $answ = `$arc -h $arc_server whoami </dev/null 2>&1`;
        unless ($answ) {
           ## print "arc did not return anything, retrying\n";
           sleep 1;
           next;
        }
        elsif ( $answ =~ /:/ ) {
           if ($answ =~ /cannot authenticate/) {
              die "No kerberos token. Please use $klog to get one\n";
           } else {
              print "error in arc command, retrying.  Error message:\n$answ";
              sleep 1;
              next;
           }
        }
        last;
     }
     $< = $R;
     ($princ) = $answ =~ /^([a-z_][\w.-]+)\@/m;
     $princ =~ s/\.$//;
     unless ($princ) { die "Unable to establish kerberos identity after several attempts. Giving up\n"; }
     print STDERR "warning: you are logged-in as $user and klog'ed as $princ\n" ,
                  " we'll process acrontab entries for $princ\n\n" unless ($user eq $princ);
   }
   my($firstl) = $princ =~ /^(.)/;
   my $i = 0; foreach (@__C_L) { last if $firstl lt $_; $i++; }
   return $__C_S[$i-1];
}


sub read_command ($@) {

    # execute a system command in a safe way and 
    # return the output in an array of strings 
    #
    # params: 
    #          $test_exit: if true, will die on a 
    #                      failed command
    #          @command  : the command to run

    my($test_exit, @command) = @_;
    my($pid, @result);
    pipe(READ, WRITE)             or die("cannot pipe: $!\n");
    defined($pid = fork())        or die("cannot fork: $!\n");
    if ($pid == 0) {
        close(READ)               or die("cannot close read: $!\n");
        open(STDIN,  '/dev/null') or die("cannot open stdin: $!\n");
        open(STDOUT, '>&WRITE')   or die("cannot open stdout: $!\n");
        open(STDERR, '>&WRITE')   or die("cannot open stderr: $!\n");
        exec({ $command[0] } @command) or die("cannot exec '@command': $!\n");
        exit(1);
    } else {
        close(WRITE)              or die("cannot close write: $!");
        @result = <READ>;
        close(READ)               or die("cannot close read: $!");
        waitpid($pid, 0) == $pid  or die("waitpid failed: $!");
        if ($test_exit) {
            $? == 0               or die("command '@command' failed\n");
        }
        return(@result);
    }
}

END {
    if (defined $CAtools'dbh) {
       $CAtools'dbh->disconnect;
    }
}

1;
__END__

=head1 NAME

CAtools - Perl interface to provide some additional AFS tools

=head1 SYNOPSIS

  use CAtools;
  $pgm=_find_afs_cmd($pgmname);
  %IPport = _get_IPports();
  @lines = get_from_oracle("select statement");
  $s = MailAddr(<user>);
  $server  = _cron_select[("user")]
  @servers = _cron_select(\*)

=head1 DESCRIPTION

This package provides some functions which used to be either "required" from
/afs/cern.ch/project/afs/etc/afsconf.pl , either cut-and-past'ed from
program to program. 

=over 4

=item B<_find_afs_cmd>

    $pgm = _find_afs_cmd(<pgmname>);

returns the full path of the specified command. The command is searched for in 
a set of "known" places, rather than from the PATH.
As the number of possible places changes, it makes only one place to update.
The _find_afs_cmd command ressembles the <fullpath> command found in Vos.pm, but 

1) doesnot use the PATH, 

2) refers to "CERN only" directories.


=item B<_get_IPports>

    %IPport = _get_IPports();

returns an array if ports used by the various AFS servers. The keys are standard names,
and the attached values the port numbers.
Special entries _<feature>_ correspond to (some of) the server features described in afsadmin.cf,
and contain a list of named ports.


=item B<get_from_oracle>

    @lines = get_from_oracle(<select statement>);

returns the result of the select statement, one field per line.
This is intended for CRA interrogations.

=item B<MailAddr>

    $string = MailAddr(<user>)

returns the mail address of a user as defined in CRA

=item B<_cron_select>

    $server  = _cron_select[(<user>)]

returns the acrontab server which will handle "user".
If user is not defined, it looks through kerberos tickets for the currently logged-on user

    @servers = _cron_select('*')

returns the list of all arontab servers


=back

=head1 AUTHORS

Bernard Antoine <Bernard.Antoine@cern.ch>

=head1 SEE ALSO

Volset and Vos modules

=cut
